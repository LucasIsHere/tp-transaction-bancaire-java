package org.example;

public class Transaction {

    public Account compteDe;
    public Account compteA;
    public double argentMontant;
    public StandardAccountOperations operation;

    public Transaction(Account compteDe, Account compteA, double argentMontant, StandardAccountOperations operation) {
        this.compteDe = compteDe;
        this.compteA = compteA;
        this.argentMontant = argentMontant;
        this.operation = operation;
    }

    @Override
    public String toString() {
        return "Transaction {compteDe= " + compteDe + ", compteA=" + compteA + ", argentMontant=" + argentMontant + ",operation=" + operation + '}';
    }
}
