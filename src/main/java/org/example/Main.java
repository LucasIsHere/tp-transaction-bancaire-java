package org.example;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        Account compte1 = new Account("Simon", 5000);
        Account compte2 = new Account("Pierre", 12340);
        Account compte3 = new Account("Jacques", 157.39);
        Account compte4 = new Account("Matthieu", 78461.33);

        System.out.println("==========  TRANSACTIONS  ==========");
        System.out.println("\n------- Simon envoie 200€ à Jacques -------");
        compte1.sendMoneyToAccount(compte3, 200);
        System.out.println("\n------- Simon envoie 250€ à Jacques -------");
        compte1.sendMoneyToAccount(compte3, 250);
        System.out.println("\n------- Simon reçoie 4000€ de Matthieu -------");
        compte1.receiveMoney(compte4, 4000);
        System.out.println("\n------- Matthieu envoie 2046.33€ à Simon -------");
        compte4.sendMoneyToAccount(compte1, 2046.33);

        System.out.println("\n==========  HISTORIQUE  ==========");
        System.out.println("\nHistorique des transactions de Simon (compte1) : " + Arrays.toString(compte1.getTransactions()));
        System.out.println("\nHistorique des transactions de Pierre (compte2) : " + Arrays.toString(compte2.getTransactions()));
        System.out.println("\nHistorique des transactions de Jacques (compte3) : " + Arrays.toString(compte3.getTransactions()));
        System.out.println("\nHistorique des transactions de Matthieu (compte4) : " + Arrays.toString(compte4.getTransactions()));

    }
}