package org.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Account {

    public static int id;
    public String name;
    public double solde;
    public Transaction[] transactions = new Transaction[0];

    public Account(String name, double solde) {
        this.name = name;
        this.solde = solde;
    }

    public Transaction[] getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        Transaction[] tabTransactions = new Transaction[0];
        tabTransactions = transactions.toArray(tabTransactions);
        this.transactions = tabTransactions;
    }

    public void sendMoneyToAccount(Account accountTo, double montantArgent) {
        accountTo.solde += montantArgent;
        removeMoney(montantArgent);

        System.out.println("Nouveau solde : " + solde + "€");
        System.out.println("Solde receiver " + accountTo.name + " : " + accountTo.solde + "€");

        List<Transaction> transactions = new ArrayList<>(Arrays.asList(this.transactions));
        Transaction newTransaction = new Transaction(this, accountTo, montantArgent, StandardAccountOperations.MONEY_TRANSFER_SEND);
        transactions.add(newTransaction);
        this.transactions = transactions.toArray(this.transactions);

        List<Transaction> transactionsAccountTo = new ArrayList<>(Arrays.asList(accountTo.getTransactions()));
        Transaction newTransaction2 = new Transaction(this, accountTo, montantArgent, StandardAccountOperations.MONEY_TRANSFER_RECEIVE);
        transactionsAccountTo.add(newTransaction2);
        accountTo.setTransactions(transactionsAccountTo);
    }

    public void receiveMoney(Account accountFrom, double moneyAmount) {
        solde += moneyAmount;
        accountFrom.removeMoney(moneyAmount);
        System.out.println("Nouveau solde : " + solde + "€");
        System.out.println("Solde emitter " + accountFrom.name + " : " + accountFrom.solde + "€");

        List<Transaction> transactions = new ArrayList<>(Arrays.asList(this.transactions));
        Transaction newTransaction = new Transaction(accountFrom, this, moneyAmount, StandardAccountOperations.MONEY_TRANSFER_RECEIVE);
        transactions.add(newTransaction);
        this.transactions = transactions.toArray(this.transactions);

        List<Transaction> transactionsAccountFrom = new ArrayList<>(Arrays.asList(accountFrom.getTransactions()));
        Transaction newTransaction2 = new Transaction(accountFrom, this, moneyAmount, StandardAccountOperations.MONEY_TRANSFER_SEND);
        transactionsAccountFrom.add(newTransaction2);
        accountFrom.setTransactions(transactionsAccountFrom);
    }

    public void removeMoney(double moneyAmount) {
        this.solde -= moneyAmount;
    }

    @Override
    public String toString() {
        return "{name='" + name + "'}";
    }
}
